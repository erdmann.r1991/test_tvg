define('overlay',['jquery'], function overlay($) {

    var $overlay;
	

    var toggleOverlay = function toggleOverlay() {
        $overlay.toggle();
        $overlay.css('marginTop', $(window).scrollTop());
        var overlayVisible = $overlay.css('display') !== 'none';
        if (overlayVisible) {
            disableScrolling();
        } else {
            enableScrolling();
        }
    };

    var setNewOverlayImage = function setNewOverlayImage(src, index) {
        $overlay.find('img').attr('src', '');
        $overlay.find('img').attr('src', src);
		$overlay.attr('data-index', index);
		
    };

    var imageclickHandler = function imageclickHandler(event) {
        var $img = $(event.target);
		var $index = $(this).attr('id').split('-');
        setNewOverlayImage($img.attr('data-originsrc'), $index[1]);
        toggleOverlay();
    };

    var disableScrolling = function disableScrolling() {
        $('html').css({
            overflow: 'hidden',
            height: '100%'
        });
    };

    var enableScrolling = function enableScrolling() {
        $('html').css({
            overflow: 'auto',
            height: 'auto'
        });
    };

    var init = function init() {
        $overlay = $('.overlay');
        $('.image-container').on('click', imageclickHandler);
        $('.prev').on('click', prev);
        $('.next').on('click', next);
        $('.close').on('click', toggleOverlay);
        switchGallery();
		
    };
	
	var prev = function prev(){
		var $newId = parseInt($overlay.attr('data-index')) - 1;
		if($newId >= 0){
			var $src =  $('#item-'+$newId).find('img').attr('data-originsrc');
			setNewOverlayImage($src, $newId);
		}
	}
	
	var next = function next(){
		var $imageLength = $('.image-grid.on .image-container').length;
		
		
		var $newId = parseInt($overlay.attr('data-index')) + 1;
		if($newId <= parseInt($imageLength - 1)){
			var $src =  $('#item-'+$newId).find('img').attr('data-originsrc');
			setNewOverlayImage($src, $newId);
		}
	}
	
	var switchGallery = function switchGallery(event){
		$('nav div').click(function(){
			$('.image-grid').removeClass('on');
			$('.image-grid.'+$(this).attr('class')).addClass('on');
		})
	}

    return {
        init: init
    };
});
