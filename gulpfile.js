'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');

var imageResize = require('gulp-image-resize');
var parallel = require("concurrent-transform");
var os = require("os");

gulp.task('sass', function () {
  return gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
});
 
gulp.task('watch', function () {
  gulp.watch('./sass/*.scss', ['sass']);
});

gulp.task('resize', function () {
	
    gulp.src('./public/images/*.{jpg,png,JPG}')
        .pipe(parallel(
            imageResize({ width : 480 }),
            imageResize({ width : 1000 }),
            os.cpus().length
        ))
        .pipe(gulp.dest('./public/images/480/'))
        .pipe(gulp.dest('./public/images/1000/'))
		;
		
		
	 gulp.src('./public/images/*.{jpg,png,JPG}')
        .pipe(parallel(
            imageResize({ width : 480 }),
            imageResize({ width : 1000 }),
            os.cpus().length
        ))
        .pipe(gulp.dest('./public/images/gallery1/'))
        .pipe(gulp.dest('./public/images/gallery1/480/'))
        .pipe(gulp.dest('./public/images/gallery1/1000/'));
		
		
	gulp.src('./public/images/*.{jpg,png,JPG}')
        .pipe(parallel(
            imageResize({ width : 480 }),
            imageResize({ width : 1000 }),
            os.cpus().length
        ))
        .pipe(gulp.dest('./public/images/gallery2/'))
        .pipe(gulp.dest('./public/images/gallery2/480/'))
        .pipe(gulp.dest('./public/images/gallery2/1000/'));		
});